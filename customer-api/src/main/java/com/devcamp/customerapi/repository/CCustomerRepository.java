package com.devcamp.customerapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerapi.model.CCustomer;

public interface CCustomerRepository extends JpaRepository<CCustomer, Integer> {

}
