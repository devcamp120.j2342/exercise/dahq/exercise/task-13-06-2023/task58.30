package com.devcamp.customerapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerapi.model.CCustomer;
import com.devcamp.customerapi.repository.CCustomerRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CCustomerController {
    @Autowired
    CCustomerRepository cCustomerRepository;

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomer() {
        try {
            List<CCustomer> cCustomers = new ArrayList<CCustomer>();
            cCustomerRepository.findAll().forEach(cCustomers::add);
            return new ResponseEntity<>(cCustomers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
